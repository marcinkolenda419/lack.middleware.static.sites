(in-package :cl-user)
(defpackage lack.middleware.static.sites
  (:use :cl)
  (:import-from :trivial-mimes
                :mime)
  (:import-from :local-time
                :format-rfc1123-timestring
                :universal-to-timestamp)
  (:import-from :uiop
                :file-exists-p
                :directory-exists-p)
  (:import-from :alexandria
                :starts-with-subseq)
  (:export :*lack-middleware-static-sites*))
(in-package :lack.middleware.static.sites)

(defun static-site-dirs (directory)
  (uiop/filesystem:subdirectories directory))

(defun site-name (path)
  (car (last (pathname-directory path))))

(defun path-re-from-name (name &key (prefix "/sites/"))
  (concatenate 'string "^" prefix "?(" name "/.*" "$)"))

(defun make-static-sites-paths (directory &optional prefix)
  (mapcar (lambda (name)
	    (if prefix
		(path-re-from-name name :prefix prefix)
		(path-re-from-name name)))
	  (mapcar #'site-name (static-site-dirs directory))))

(defun lack-middleware-static-sites (app env &key directory (prefix nil prefix-p))
  (block b1
    (dolist (path (apply
		   #'make-static-sites-paths
		   directory
		   (when prefix-p (list prefix))))
      (let* ((path-info (getf env :path-info)))
	(multiple-value-bind (matched relative)
	    (ppcre:scan-to-strings path path-info)
	  (if matched		
	      (progn
		(setf (getf env :path-info)
		      (aref relative 0))
		(return-from b1 (call-app-file directory env)))))))
    (progn
      (funcall app env))))

(defparameter *lack-middleware-static-sites*
  (lambda (app &key directory (prefix nil prefix-p))
    (lambda (env)
      (apply #'lack-middleware-static-sites app env
	     (append (when directory (list :directory directory))
		     (when prefix-p (list :prefix prefix))))))
  "Middleware for serving static sites.")

(defun call-app-file (root env)
  (funcall (make-app :root root) env))


(define-condition bad-request (simple-condition) ())
(define-condition not-found (simple-condition) ())

(defun make-app (&key file (root #P"./") (encoding "utf-8"))
  (lambda (env)
    (handler-case
        (serve-path
         (locate-file (or file
                          (getf env :path-info))
                      root)
         encoding env)
      (bad-request ()
        '(400 (:content-type "text/plain"
               :content-length 11)
          ("Bad Request")))
      (not-found ()
        '(404 (:content-type "text/plain"
               :content-length 9)
          ("Not Found"))))))

(defun locate-file (path root)
  (when (find :up (pathname-directory path) :test #'eq)
    (error 'bad-request))

  (let ((file (merge-pathnames path root)))
    (cond
      ((position #\Null (namestring file))
       (error 'bad-request))
      ((not (or (ignore-errors
		  ;; Ignore simple-file-error in a case that
		  ;; the file path contains some special characters like "?".
		  ;; See https://github.com/fukamachi/clack/issues/111
		  (uiop:file-exists-p file))
		(uiop:directory-exists-p file)))
       (error 'not-found))
      (t file))))

(djula:add-template-directory
 (asdf:system-source-directory "lack.middleware.static.sites"))

(defparameter *dir-listing-template* nil)

(defun env-entries (dir)
  ""
  (let ((entries nil))
    (dolist (file (uiop/filesystem:directory-files dir))
      (let ((entry '()))
	(setf (getf entry :url) (file-namestring file))
	(setf (getf entry :name) (file-namestring file))
	(pushnew entry entries)))
    entries))

(defun serve-directory (dir encoding env)
  (unless *dir-listing-template*
    (setf *dir-listing-template* (djula:compile-template* "dir-listing.html")))
  (setf (getf env 'entries) (env-entries dir))
  (let ((body (apply #'djula:render-template*
		     *dir-listing-template*	 nil
		     env)))
    `(200
      (:content-type "text/html"
		     :content-length ,(length body)
		     :last-modified
		     ,(format-rfc1123-timestring nil
						 (universal-to-timestamp
						  (get-universal-time))))
      (,body))))

(defun serve-path (file encoding env)
  (if (uiop:directory-exists-p file)
      (serve-directory file encoding env)
      (let ((content-type (or (mimes:mime-lookup file)
			      "application/octet-stream"))
	    (univ-time (or (file-write-date file)
			   (get-universal-time))))
	(when (starts-with-subseq "text" content-type)
	  (setf content-type
		(format nil "~A~:[~;~:*; charset=~A~]"
			content-type encoding)))
	(with-open-file (stream file
				:direction :input
				:if-does-not-exist nil)
	  `(200
	    (:content-type ,content-type
			   :content-length ,(file-length stream)
			   :last-modified
			   ,(format-rfc1123-timestring nil
						       (universal-to-timestamp univ-time)))
	    ,file)))))
