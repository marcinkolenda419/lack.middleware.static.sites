(defsystem "lack.middleware.static.sites"
  :version "0.1.0"
  :author "Marcin Kolenda"
  :license ""
  :depends-on ("clack"
               "lack"
               "caveman2"
               "envy"
               "cl-ppcre"
               "uiop"
	       "local-time"
	       "trivial-mimes"
	       "alexandria"
	       "cl-ppcre"
	       
               ;; for @route annotation
               "cl-syntax-annot"

               ;; HTML Template
               "djula")
  :components ((:file "lack.middleware.static.sites"))
  :description "")
